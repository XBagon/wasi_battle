(module
  (type (;0;) (func))
  (type (;1;) (func (param i32) (result i32)))
  (func $__wasm_call_ctors (type 0))
  (func $other (type 1) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    local.set 1
    i32.const 16
    local.set 2
    local.get 1
    local.get 2
    i32.sub
    local.set 3
    i32.const 0
    local.set 4
    local.get 4
    local.set 5
    local.get 0
    local.set 6
    local.get 5
    local.get 6
    i32.le_s
    local.set 7
    i32.const 1
    local.set 8
    local.get 7
    local.get 8
    i32.and
    local.set 9
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 9
          i32.eqz
          br_if 0 (;@3;)
          i32.const 2147483647
          local.set 19
          local.get 0
          local.set 20
          local.get 19
          local.set 21
          local.get 20
          local.get 21
          i32.le_s
          local.set 22
          i32.const 1
          local.set 23
          local.get 22
          local.get 23
          i32.and
          local.set 24
          local.get 24
          br_if 1 (;@2;)
        end
        i32.const -1
        local.set 13
        local.get 0
        local.set 14
        local.get 13
        local.set 15
        local.get 14
        local.get 15
        i32.eq
        local.set 16
        i32.const 1
        local.set 17
        local.get 16
        local.get 17
        i32.and
        local.set 18
        block  ;; label = @3
          local.get 18
          br_if 0 (;@3;)
          i32.const 1
          local.set 12
          local.get 3
          local.get 12
          i32.store8 offset=15
          br 2 (;@1;)
        end
        i32.const 2
        local.set 11
        local.get 3
        local.get 11
        i32.store8 offset=15
        br 1 (;@1;)
      end
      i32.const 0
      local.set 10
      local.get 3
      local.get 10
      i32.store8 offset=15
    end
    local.get 3
    i32.load8_u offset=15
    local.set 25
    local.get 25
    return)
  (table (;0;) 1 1 funcref)
  (memory (;0;) 16)
  (global (;0;) (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1048576))
  (global (;2;) i32 (i32.const 1048576))
  (export "memory" (memory 0))
  (export "__heap_base" (global 1))
  (export "__data_end" (global 2))
  (export "other" (func $other)))
