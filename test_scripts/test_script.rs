#[no_mangle]
pub fn other(x: i32) -> Doot {
    let y = match x {
        0..=std::i32::MAX => Doot::A,
        -1 => Doot::C,
        _ => Doot::B,
    };

    /*
    unsafe {
        test();
    };
    */

    y
}

pub enum Doot {
    A,
    B,
    C,
}

/*
extern "C" {
    fn test();
}
*/
