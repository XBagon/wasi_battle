use cranelift_codegen::settings;
use cranelift_codegen::settings::Configurable;
use cranelift_entity::PrimaryMap;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::io::BufRead;
use std::rc::Rc;
use wasmtime_jit::{instantiate, Compiler, NullResolver, Resolver, RuntimeValue};
use wasmtime_runtime::{Export, Imports};

const DATA: &'static [u8] = include_bytes!("../test_scripts/test_script.wasm");

fn main() {
    let mut flag_builder = settings::builder();
    flag_builder.enable("enable_verifier").unwrap();
    let isa_builder = cranelift_native::builder().unwrap_or_else(|_| {
        panic!("host machine is not a supported target");
    });
    let isa = isa_builder.finish(cranelift_codegen::settings::Flags::new(flag_builder));
    let mut resolver = NullResolver {};
    let _imports = Imports::new(
        HashSet::new(),
        PrimaryMap::new(),
        PrimaryMap::new(),
        PrimaryMap::new(),
        PrimaryMap::new(),
    );
    let mut compiler = Compiler::new(isa);
    let global_exports = Rc::new(RefCell::new(HashMap::new()));
    let mut instance =
        instantiate(&mut compiler, &DATA, &mut resolver, global_exports, true).unwrap();
    let mut context = wasmtime_jit::Context::new(Box::new(compiler));
    let stdin = std::io::stdin();
    for line in stdin.lock().lines() {
        dbg!(context
            .invoke(
                &mut instance,
                "other",
                &[RuntimeValue::I32(line.unwrap().parse().unwrap())]
            )
            .unwrap());
    }
}
